

A Pass Phrase Generator
Rather than using random letters, numbers and special characters to create a password, a pass phrase consisting of a series of words can be more secure and memorable. 

Getting Started
You can clone this and run it with Python.

Prerequisites
Python

Author
BFCY

References
Background info and wordlist from:
https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases
http://www.webplaces.com/passwords/passphrase-word-lists.htm
